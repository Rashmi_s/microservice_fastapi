from typing import List

import uvicorn
from fastapi import FastAPI, APIRouter, HTTPException
from app.api import router
from pydantic import BaseModel
api_router=APIRouter()

app=FastAPI(title="Movie Project", openapi_url="/api/v1/openapi.json")

api_router.include_router(router, prefix="/movie", tags=["Movie"])

app.include_router(api_router)






if __name__ == "__main__":
    uvicorn.run(app,host="0.0.0.0", port=8087,debug=True)