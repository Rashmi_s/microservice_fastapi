from fastapi import FastAPI, APIRouter, Depends
from sqlalchemy.orm import Session

from app.crud import get_all_movie
from app.db import get_db

router = APIRouter()


@router.get("/")
def get_all(
        db_session: Session = Depends(get_db),
        limit: int = 100,
        offset: int = 0
):
    return get_all_movie(db_session=db_session, limit=limit, offset=offset)
