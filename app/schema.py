from pydantic.main import BaseModel


class MovieCreateSchema(BaseModel):
    name:str
    generos:str
    cast_id:int

class MovieUpdateSchema(MovieCreateSchema):
    pass

class MovieReturnSchema(MovieCreateSchema):
    id:int
