from sqlalchemy.orm import Session

from app.models import Movie


async def get_all_movie(
        db_session: Session,
        limit: int = None,
        offset: int = None
):
    _result = await db_session.query(Movie)

    if limit:
        _result = await _result.filter(limit=limit)
    if offset:
        _result = await _result.filter(offset=offset)
    result = await _result.all()
    return result

