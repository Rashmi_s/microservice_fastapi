from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from starlette.requests import Request


def get_db(request: Request):
    return request.state.db


SQLALCHEMY_DATABASE_URI = "mysql+pymysql://admin_movie1:Admin_movie%1234@ms-movie1.cihx2ytlyzd6.ap-southeast-1.rds.amazonaws.com:3306/movie_ms?charset=utf8"
engine = create_engine(SQLALCHEMY_DATABASE_URI, pool_pre_ping=True, pool_size=40, max_overflow=0)
db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine)
)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)




