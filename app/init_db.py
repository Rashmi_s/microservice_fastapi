from app.db import engine, db_session
from app.models import Base


def init_db(db_session):

    Base.metadata.create_all(bind=engine)


if __name__ == '__main__':
    init_db(db_session=db_session)
