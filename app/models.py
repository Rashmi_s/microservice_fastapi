from sqlalchemy import Column, String
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

metadata = Base.metadata


class Movie(Base):
    __tablename__ = "movie"
    id = Column(BIGINT(10), primary_key=True)
    name = Column(String(100), nullable=False)
    generos = Column(String(100))
    cast_id = Column(BIGINT(10))

